-- Возможные опции
-- --channel=cc.ru
-- --server=stem.fomalhaut.me
-- --port=5733
-- --timezone=3
-- --logpath=/tmp/rrct.log
-- --loglevel=1
-- --reconnect-tryes=5 - значение inf заставит переподключаться бесконечно
-- -d - работа в фоне
-- -f - писать лог в файл

local CHANNEL = "cc.ru"
local SERVER = "stem.fomalhaut.me"
local PORT = 5733
local TIMEZONE = 3
local LOGPATH = "/tmp/rrct.log"
local LOGLEVEL = 1
local RECONNECT_TRYES = 5
local RECONNECT_DELAY = 5

local logLevels = {
    [6] = "FATAL",
    [5] = "CRIT",
    [4] = "ERROR",
    [3] = "WARN",
    [2] = "INFO",
    [1] = "DEBUG"
}

local args, options = require("shell").parse(...)
local serialization = require("serialization")
local json = require("json")
local event = require("event")
local component = require("component")
local robot = require("robot")
local filesystem = require("filesystem")
local raw_robot = component.robot

local channel = options["channel"] or CHANNEL
local server = options["server"] or SERVER
local port = options["port"] or PORT
local timezone = tonumber(options["timezone"]) or TIMEZONE
local logPath = options["logpath"] or LOGPATH
local logLevel = tonumber(options["loglevel"]) or LOGLEVEL
local reconnect_tryes = options["reconnect-tryes"] == "inf" and math.huge or tonumber(options["reconnect-tryes"]) or RECONNECT_TRYES

local robotChannel = ("%s-robot-%s"):format(channel, component.internet.address)

local stem, pingTimer

local function jsonSend(channel, table)
    stem:send(channel, json.encode(table))
end

local function reconnect()
    stem = stem and stem:reconnect() or (require("stem").connect(server, port))

    if stem then
        stem:subscribe(channel)
        stem:subscribe(robotChannel)
    else
        return false
    end

    return true
end

local function getTimestamp()
	local file = io.open("/tmp/time", "w")
	file:write("")
	file:close()

	return filesystem.lastModified("/tmp/time") / 1000 + 3600 * timezone
end

local function log(message, level)
    if level >= logLevel then
        local timestamp = getTimestamp()
        message = ("%s %s"):format(logLevels[level], tostring(message))
        local time = os.date("%H:%M:%S ", timestamp)
        local fullTime = os.date("%d.%m.%Y %H:%M:%S ", timestamp)

        if not options["d"] then
            if logLevel >= 4 then
                io.stderr:write(time .. message)
            else
                print(time .. message)
            end
        end

        if options["f"] then
            local file = io.open(logPath, "a")

            if file then
                file:write(fullTime .. message .. "\n")
                file:close()
            elseif not options["d"] then
                io.stderr:write("Can't open log file for append, may be disk out of space.")
            end
        end
    end
end

local function fatal(messsage)
    log(messsage, 6)
end

local function critical(messsage)
    log(messsage, 5)
end

local function error(messsage)
    log(messsage, 4)
end

local function warn(messsage)
    log(messsage, 3)
end

local function info(messsage)
    log(messsage, 2)
end

local function debug(messsage)
    log(messsage, 1)
end

local function forward(side, distance)
    local blocksPassed = 0

    for i = 1, distance or 1 do
        if raw_robot.move(side, i) then
            blocksPassed = blocksPassed + 1
        end
    end

    return blocksPassed
end

local commands = {
    forward = function(args)
        jsonSend(args.channel, {command = "forward", result = forward(3, args.dist)})
    end,

    back = function(args)
        robot.turnAround()
        jsonSend(args.channel, {command = "back", result = forward(3, args.dist)})
        robot.turnAround()
    end,

    up = function(args)
        jsonSend(args.channel, {command = "up", result = forward(1, args.dist)})
    end,

    down = function(args)
        jsonSend(args.channel, {command = "down", result = forward(0, args.dist)})
    end,

    turnLeft = function(args)
        jsonSend(args.channel, {command = "turnLeft", result = robot.turnLeft()})
    end,

    turnRight = function(args)
        jsonSend(args.channel, {command = "turnRight", result = robot.turnRight()})
    end,

    turnAround = function(args)
        jsonSend(args.channel, {command = "turnAround", result = robot.turnAround()})
    end,

    use = function(args)
        jsonSend(args.channel, {command = "use", result = robot.use(args.side, args.sneaky)})
    end
}

local function messageHandler(_, receivedFrom, message)
    debug(("#%s %s"):format(receivedFrom, message))

    if receivedFrom == robotChannel then
        local data, err = serialization.unserialize(message)

        if data then
            if commands[data.command] then
                debug("Running command " .. data.command)
                data.channel = receivedFrom
                commands[data.command](data)
            end
        else
            warn(("Can't unserialize message '%s' %s"):format(message, err or ""))
        end
    elseif receivedFrom == channel then
        if message == "0" then
            stem:send(channel, robotChannel)
        end
    end
end

pingTimer = event.timer(20, function()
    if not stem:ping("PING") then
        warn("Stem server doesn't respond to ping message, trying to reconnect...")

        if reconnect() then
            info("Reconnection successful")
        else
            for i = 1, reconnect_tryes do
                debug(("%s/%s reconnection attempt"):format(i, reconnect_tryes == math.huge and "INF" or reconnect_tryes))
                os.sleep(RECONNECT_DELAY)

                if reconnect() then
                    info("Reconnected successful")
                    break
                elseif i == reconnect_tryes then
                    fatal("Reconnection failed")
                    exit()
                end
            end
        end
    end
end, math.huge)

function exit()
    info("Exitting...")

    if stem then 
        stem:disconnect()
    end

    event.ignore("stem_message", messageHandler)
    event.cancel(pingTimer)
    exit = nil
    os.exit()
end

info(("Connecting to %s:%s"):format(server, port))
if reconnect() then
    info("Connected!")
else
    fatal("Connection failed")
    exit()
end

if options["d"] then
    event.listen("stem_message", messageHandler)
    info("RRCT client started and forked to background")
else
    require("process").info().data.signal = function()
        exit()
    end

    info("RRCT client started")

    while true do 
        messageHandler(_, select(2, event.pull("stem_message")))
    end
end