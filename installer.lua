local internet = require("component").internet
local files = {
    {url = "https://gitlab.com/UnicornFreedom/stem/raw/master/stem.lua", path = "/lib/stem.lua"},
    {url = "https://raw.githubusercontent.com/rxi/json.lua/master/json.lua", path = "/lib/JSON.lua"},
    {url = "https://gitlab.com/cc-ru/rrct/rrct-client/raw/master/main.lua", path = "/home/rrct-client.lua"}
}

local function error(err)
    io.stderr:write(err)
    os.exit()
end

local function download(url, path)
    print("Downloading " .. path .. "...")
    local handle, data, chunk = internet.request(url, nil, {["user-agent"]="Chrome/81.0.4044.129"}), ""

    if handle then
        while true do
            chunk = handle.read()

            if chunk then
                data = data .. chunk
            else
                break
            end
        end
    else
        error("Failed to open handle for request " .. url)
    end
     
    local file = io.open(path, "w")

    if file then 
        file:write(data)
        file:close()
    else
        error("Failed to open file for writing " .. path)
    end
end

for i = 1, #files do
    download(files[i].url, files[i].path)
end
print("Installation complete!")